import React, { useEffect, useState } from "react";
import { Text, View, StyleSheet, TextInput, Button, Alert } from "react-native";
import AsyncStorage from "@react-native-async-storage/async-storage";

const StartUp = ({ navigation }) => {
  //Set Name hook
  let [name, setName] = useState("");

  //Show Alert Function
  let showAlert = () => {
    //Save the name first in Local Storage
    AsyncStorage.setItem("user", name);
    //Alert Itself
    Alert.alert(
      `Hello ${name}! Welcome to What To Do! `,
      "Tap ok to continue.",
      [
        {
          text: "Ok",
          onPress: () => {
            navigation.navigate("ConfirmDeviceScreen");
          },
          style: "default",
        },
      ]
    );
  };

  return (
    <View style={styles.container}>
      <Text style={styles.header}>Welcome to What To Do!</Text>
      <Text style={styles.subHeader}>First up lets register your device:</Text>
      <TextInput
        style={styles.input}
        onChangeText={setName}
        value={name}
        placeholder="What would you like us to call you?"
        keyboardType="default"
      />

      <Button color="#8CBBF1" title="Next" onPress={showAlert} />
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#FFFFFF",
    alignItems: "center",
    justifyContent: "center",
  },

  input: {
    height: 40,
    margin: 12,
    borderWidth: 1,
    padding: 10,
    fontSize: 16,
    textAlign: "center",
  },

  header: {
    fontSize: 24,
  },
  subHeader: {
    fontSize: 16,
  },
});

export default StartUp;
