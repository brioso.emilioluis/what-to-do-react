import React, { useState, useEffect, useContext } from "react";
import {
  KeyboardAvoidingView,
  Platform,
  StyleSheet,
  Text,
  View,
  TextInput,
  TouchableOpacity,
  Keyboard,
  Alert,
  Modal,
  Button,
} from "react-native";
import Task from "../components/Task";
import GestureRecognizer from "react-native-swipe-gestures";
import { SwipeListView } from "react-native-swipe-list-view";
import moment from "moment";
import UserContext from "../UserContext";

const TaskToday = (selectedDay) => {
  //Get UUID from user context
  const { uuid, setUuid } = useContext(UserContext);

  //Create hooks for task information
  const [name, setName] = useState("");
  const [description, setDescription] = useState("");
  const [oldTaskDescription, setOldTaskDescription] = useState("");
  const [newTaskDescription, setNewTaskDescription] = useState("");
  const [taskId, setTaskId] = useState("");
  const [isNext, setIsNext] = useState(false);
  const [date, setDate] = useState("");
  const [taskOwner, setTaskOwner] = useState(uuid);

  // Current date variables
  let today = moment().format("YYYY-MM-DD");

  //DD, MM, and Day hooks for rendering
  const [dd, setDd] = useState("");
  const [mm, setMm] = useState("");
  const [day, setDay] = useState("");

  //Create array hook to set tasks upon fetching from API
  const [taskItems, setTaskItems] = useState([]);

  //Modal State for editing tasks
  const [modal, setModal] = useState(false);
  const handleShow = (e) => setModal(e);

  //Add Task
  const handleAddTask = () => {
    Keyboard.dismiss();
    fetch(`https://fierce-crag-15789.herokuapp.com/tasks/create`, {
      method: "POST",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
      },
      body: JSON.stringify({
        name: name,
        description: description,
        date: date,
        taskOwner: taskOwner,
      }),
    })
      .then((res) => res.json())
      .then((data) => {
        if (data === true) {
          Alert.alert("Success!", "You have successfully added a new task!", [
            {
              text: "Ok",
              onPress: () => {
                setName("");
                setDescription("");
                setIsNext(false);
              },
            },
          ]);
        }
      });
  };

  //Mark task as done function
  const completeTask = (id) => {
    console.log(id);
    Alert.alert("Mark task as done?", "Tapping the task marks it as done.", [
      { text: "Cancel", style: "cancel" },
      {
        text: "Ok",
        onPress: () => {
          fetch(`https://fierce-crag-15789.herokuapp.com/tasks/done`, {
            method: "POST",
            headers: {
              Accept: "application/json",
              "Content-Type": "application/json",
            },
            body: JSON.stringify({
              taskId: id,
              taskOwner: taskOwner,
            }),
          });
        },
      },
    ]);
  };

  // Open Modal for editing task
  const openModal = (task) => {
    setModal(true);
    setName(task.name);
    setTaskId(task._id);
    setOldTaskDescription(task.description);
  };

  // Edit Task
  const editTask = () => {
    fetch(`https://fierce-crag-15789.herokuapp.com/tasks/edit`, {
      method: "POST",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
      },
      body: JSON.stringify({
        taskId: taskId,
        name: name,
        date: date,
        taskOwner: taskOwner,
        oldTaskDescription: oldTaskDescription,
        newTaskDescription: newTaskDescription,
      }),
    })
      .then((res) => res.json())
      .then((res) => {
        if (res == true) {
          Alert.alert(
            "Success!",
            "You have successfully edited task description!",
            [{ text: "Ok", onPress: () => setModal(false) }]
          );
        }
      });
  };

  // Delete Task
  const deleteTask = (task) => {
    fetch(`https://fierce-crag-15789.herokuapp.com/tasks/delete`, {
      method: "DELETE",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
      },
      body: JSON.stringify({
        taskId: task._id,
        taskOwner: task.taskOwner,
      }),
    })
      .then((res) => res.json())
      .then((res) => {
        if (res == true) {
          Alert.alert("Success!", "You have successfully deleted the task!", [
            { text: "Ok" },
          ]);
        }
      });
  };

  const months = [
    "January",
    "February",
    "March",
    "April",
    "May",
    "June",
    "July",
    "August",
    "September",
    "October",
    "November",
    "December",
  ];

  const days = [
    "Sunday",
    "Monday",
    "Tuesday",
    "Wednesday",
    "Thursday",
    "Friday",
    "Saturday",
  ];

  //Set date upon app open
  useEffect(() => {
    if (selectedDay.route.params === undefined) {
      setDate(today);
      setDd(moment().format("DD"));
      setMm(moment().format("M") - 1);
      setDay(moment().format("d"));
    } else {
      setDate(selectedDay.route.params.day.dateString);
      setDd(selectedDay.route.params.day.day);
      setMm(selectedDay.route.params.day.month - 1);
      setDay(moment(selectedDay.route.params.day.dateString).format("d"));
      console.log(`log from useEffect: ${date}`);
    }
  }, [selectedDay]);

  //Fetch today's tasks from API and set it taskItems
  useEffect(() => {
    fetch(`https://fierce-crag-15789.herokuapp.com/tasks/today`, {
      method: "POST",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
      },
      body: JSON.stringify({
        taskOwner: taskOwner,
        date: date,
      }),
    })
      .then((res) => res.json())
      .then((data) => {
        setTaskItems(data);
      });
  }, [date, taskOwner, taskItems]);

  //Render TaskToday page
  return (
    <View style={styles.container}>
      {/* Today's Task */}
      <View style={styles.tasksWrapper}>
        <Text style={styles.sectionTitle}>Today's Tasks:</Text>
        <Text style={styles.sectionTitle}>
          {months[mm]} {dd}, {days[day]}
        </Text>

        <SwipeListView
          style={styles.items}
          data={taskItems}
          renderItem={(data) => {
            return (
              <TouchableOpacity
                key={data.item._id}
                onPress={() => completeTask(data.item._id)}
                onLongPress={() => openModal(data.item)}
              >
                <Task props={data} />
              </TouchableOpacity>
            );
          }}
          renderHiddenItem={(data) => {
            return (
              <View style={styles.delete}>
                <TouchableOpacity
                  style={[styles.deleteButton]}
                  onPress={() => deleteTask(data.item)}
                >
                  <Text style={styles.deleteText}>Delete</Text>
                </TouchableOpacity>
              </View>
            );
          }}
          disableRightSwipe={true}
          rightOpenValue={-75}
          previewRowKey={"0"}
          previewOpenValue={-40}
          previewOpenDelay={3000}
        />
      </View>

      {/* Write a task */}
      <KeyboardAvoidingView
        behavior={Platform.OS === "ios" ? "padding" : "height"}
        style={styles.writeTaskWrapper}
      >
        {isNext ? (
          <>
            <TextInput
              style={styles.input}
              placeholder={"Add task description"}
              value={description}
              onChangeText={(text) => setDescription(text)}
            />

            <TouchableOpacity
              onPress={() => {
                setIsNext(false);
              }}
            >
              <View style={styles.addWrapper}>
                <Text style={styles.addText}>back</Text>
              </View>
            </TouchableOpacity>
          </>
        ) : (
          <>
            <TextInput
              style={styles.input}
              placeholder={"Add a new task"}
              value={name}
              onChangeText={(text) => setName(text)}
              maxLength={240}
            />

            {name !== "" ? (
              <TouchableOpacity
                onPress={() => {
                  setIsNext(true);
                }}
              >
                <View style={styles.addWrapper}>
                  <Text style={styles.addText}>></Text>
                </View>
              </TouchableOpacity>
            ) : (
              <View></View>
            )}
          </>
        )}

        <TouchableOpacity
          onPress={() => {
            handleAddTask();
          }}
        >
          <View style={styles.addWrapper}>
            <Text style={styles.addText}>+</Text>
          </View>
        </TouchableOpacity>
      </KeyboardAvoidingView>

      {/* Modal */}
      <GestureRecognizer
        style={{ flex: 1 }}
        onSwipeDown={() => setModal(false)}
      >
        <Modal
          animationType="slide"
          visible={modal}
          onHide={() => handleShow(false)}
        >
          <View style={styles.modalContainer}>
            <Text style={styles.sectionTitle}>Edit Task:{name}</Text>
            <Text style={styles.sectionSubTitle}> CHANGE TASK DESCRIPTION</Text>
            {}
            <TextInput
              style={styles.input}
              placeholder={"Please put new task description"}
              value={newTaskDescription}
              onChangeText={(text) => setNewTaskDescription(text)}
              maxLength={240}
            />
            <Button onPress={() => editTask()} title="UPDATE" />
          </View>
        </Modal>
      </GestureRecognizer>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#FFFF",
  },
  modalContainer: {
    flex: 1,
    backgroundColor: "#E8EAED",
    flexDirection: "column",
    justifyContent: "center",
    alignItems: "center",
  },
  tasksWrapper: {
    paddingTop: 30,
    paddingHorizontal: 20,
  },
  sectionTitle: {
    fontSize: 24,
    fontWeight: "bold",
  },
  sectionSubTitle: {
    fontSize: 16,
    marginVertical: 15,
  },
  items: {
    marginTop: 30,
  },
  writeTaskWrapper: {
    position: "absolute",
    bottom: 60,
    width: "100%",
    flexDirection: "row",
    justifyContent: "space-around",
    alignItems: "center",
  },
  input: {
    paddingVertical: 15,
    paddingHorizontal: 15,
    width: 250,
    backgroundColor: "#FFF",
    borderRadius: 60,
    borderColor: "#C0C0C0",
    borderWidth: 1,
    fontSize: 16,
    textAlign: "center",
    marginBottom: 15,
  },
  addWrapper: {
    width: 60,
    height: 60,
    backgroundColor: "#FFF",
    borderRadius: 60,
    justifyContent: "center",
    alignItems: "center",
    borderColor: "#C0C0C0",
    borderWidth: 1,
  },
  modalButton: {
    width: 120,
    height: 120,
  },
  delete: {
    alignItems: "center",
    borderRadius: 10,
    backgroundColor: "#DDD",
    flexDirection: "row",
    justifyContent: "space-between",
    padding: 15,
    height: "auto",
  },
  deleteButton: {
    alignItems: "center",
    bottom: 0,
    justifyContent: "center",
    position: "absolute",
    top: 0,
    width: 75,
    backgroundColor: "red",
    right: 0,
    height: 65,
    borderTopRightRadius: 10,
    borderBottomRightRadius: 10,
  },
  deleteText: {
    color: "#FFF",
  },
});

export default TaskToday;
