import {
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
  Modal,
  TextInput,
  Alert,
  Button,
} from "react-native";
import React, { Component, useEffect, useState, useContext } from "react";
import AsyncStorage from "@react-native-async-storage/async-storage";
import GestureRecognizer from "react-native-swipe-gestures";
import UserContext from "../UserContext";

const SettingsScreen = () => {
  //Get UUID from user context
  const { uuid, setUuid } = useContext(UserContext);

  //Change Task Day Colors
  const { taskDayColor, setTaskDayColor } = useContext(UserContext);
  const [taskDayModal, setTaskDayModal] = useState(false);
  const handleShowTaskDayModal = (e) => setTaskDayModal(e);
  const changeTaskDayColor = (color) => {
    console.log(color);
    fetch(`https://fierce-crag-15789.herokuapp.com/users/changeFreeDay`, {
      method: "POST",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
      },
      body: JSON.stringify({
        userId: uuid,
        oldColor: taskDayColor,
        newColor: color,
      }),
    })
      .then((res) => res.json())
      .then((res) => {
        setTaskDayColor(color);
      });
  };

  //Change Current Day Colors
  const { currentDayColor, setCurrentDayColor } = useContext(UserContext);
  const [currentDayModal, setCurrentDayModal] = useState(false);
  const handleShowCurrentDayModal = (e) => setCurrentDayModal(e);
  const changeCurrentDayColor = (color) => {
    console.log(color);
    setCurrentDayColor(color);
    fetch(`https://fierce-crag-15789.herokuapp.com/users/changeCurrentDay`, {
      method: "POST",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
      },
      body: JSON.stringify({
        userId: uuid,
        oldColor: currentDayColor,
        newColor: color,
      }),
    })
      .then((res) => res.json())
      .then((res) => {
        setCurrentDayColor(color);
      });
  };

  //Delete user Data
  const [deleteUserModal, setDeleteUserModal] = useState(false);
  const handleShowDeleteUserModal = (e) => setDeleteUserModal(e);
  const deleteUser = () => {
    console.log(`delete button`);
    fetch(`https://fierce-crag-15789.herokuapp.com/users/deleteUser`, {
      method: "DELETE",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
      },
      body: JSON.stringify({
        userId: uuid,
      }),
    })
      .then((res) => res.json())
      .then((res) => {
        Alert.alert("Success!", "You have successfully deleted user data!", [
          {
            text: "Ok",
            onPress: () => {
              AsyncStorage.clear();
              setUuid(null);
            },
          },
        ]);
      });
  };
  return (
    <View style={styles.container}>
      <View style={styles.subContainer}>
        <Text style={styles.title}>UUID: </Text>
        <Text style={styles.subTitle}> {uuid} </Text>
      </View>
      <View style={styles.subContainer}>
        <Text style={styles.title}>What to do Version: </Text>
        <Text style={styles.subTitle}>1.0.0 </Text>
      </View>
      <View style={styles.subContainer}>
        <TouchableOpacity
          onPress={() => {
            console.log("change color for task days");
            setTaskDayModal(true);
          }}
        >
          <View style={styles.touchContainer}>
            <Text style={styles.touchText}>Change color for task days</Text>
          </View>
        </TouchableOpacity>
      </View>
      <View style={styles.subContainer}>
        <TouchableOpacity
          onPress={() => {
            console.log("change color for current day");
            setCurrentDayModal(true);
          }}
        >
          <View style={styles.touchContainer}>
            <Text style={styles.touchText}>Change color for current day</Text>
          </View>
        </TouchableOpacity>
      </View>

      <View style={styles.subContainer}>
        <TouchableOpacity
          onPress={() => {
            console.log("delete user");
            setDeleteUserModal(true);
          }}
        >
          <View style={styles.deleteContainer}>
            <Text style={styles.touchText}>Delete user data</Text>
          </View>
        </TouchableOpacity>
      </View>

      {/* Set Task Day Color Modal */}
      <GestureRecognizer
        style={{ flex: 1 }}
        onSwipeDown={() => setTaskDayModal(false)}
      >
        <Modal
          animationType="slide"
          visible={taskDayModal}
          onHide={() => handleShowTaskDayModal(false)}
        >
          <View style={styles.closeButtonLoc}>
            <TouchableOpacity
              onPress={() => {
                handleShowTaskDayModal(false);
              }}
            >
              <View style={styles.modalCloseButton}>
                <Text>x</Text>
              </View>
            </TouchableOpacity>
          </View>
          <View style={styles.modalContainer}>
            <Text style={styles.modalTitle}>Change Task Day Color</Text>
            <TouchableOpacity onPress={() => changeTaskDayColor("#FDE18D")}>
              <View style={styles.taskDayColor1}>
                <Text style={styles.modalTitle}>#FDE18D</Text>
              </View>
            </TouchableOpacity>
            <TouchableOpacity onPress={() => changeTaskDayColor("#3D7D0B")}>
              <View style={styles.taskDayColor2}>
                <Text style={styles.modalTitle}>#3D7D0B</Text>
              </View>
            </TouchableOpacity>
            <TouchableOpacity onPress={() => changeTaskDayColor("#E59010")}>
              <View style={styles.taskDayColor3}>
                <Text style={styles.modalTitle}>#E59010</Text>
              </View>
            </TouchableOpacity>
          </View>
        </Modal>
      </GestureRecognizer>

      {/* Set Current Day Color Modal */}
      <GestureRecognizer
        style={{ flex: 1 }}
        onSwipeDown={() => setCurrentDayModal(false)}
      >
        <Modal
          animationType="slide"
          visible={currentDayModal}
          onHide={() => handleShowCurrentDayModal(false)}
        >
          <View style={styles.closeButtonLoc}>
            <TouchableOpacity
              onPress={() => {
                handleShowCurrentDayModal(false);
              }}
            >
              <View style={styles.modalCloseButton}>
                <Text>x</Text>
              </View>
            </TouchableOpacity>
          </View>
          <View style={styles.modalContainer}>
            <Text style={styles.modalTitle}>Change Current Day Color</Text>
            <TouchableOpacity onPress={() => changeCurrentDayColor("#D7DDE9")}>
              <View style={styles.currentDayColor1}>
                <Text style={styles.modalTitle}>#D7DDE9</Text>
              </View>
            </TouchableOpacity>
            <TouchableOpacity onPress={() => changeCurrentDayColor("#76A6F0")}>
              <View style={styles.currentDayColor2}>
                <Text style={styles.modalTitle}>#76A6F0</Text>
              </View>
            </TouchableOpacity>
            <TouchableOpacity onPress={() => changeCurrentDayColor("#8CBBF1")}>
              <View style={styles.currentDayColor3}>
                <Text style={styles.modalTitle}>#8CBBF1</Text>
              </View>
            </TouchableOpacity>
          </View>
        </Modal>
      </GestureRecognizer>

      {/* Delete User Modal */}
      <GestureRecognizer
        style={{ flex: 1 }}
        onSwipeDown={() => setDeleteUserModal(false)}
      >
        <Modal
          animationType="slide"
          visible={deleteUserModal}
          onHide={() => handleShowDeleteUserModal(false)}
        >
          <View style={styles.closeButtonLoc}>
            <TouchableOpacity
              onPress={() => {
                handleShowDeleteUserModal(false);
              }}
            >
              <View style={styles.modalCloseButton}>
                <Text>x</Text>
              </View>
            </TouchableOpacity>
          </View>
          <View style={styles.modalContainer}>
            <Text style={styles.modalTitle}>
              Are you sure you want to delete user data?
            </Text>
            <Text style={styles.modalSubTitle}>
              (All tasks will be deleted)
            </Text>
            <Button
              title="Delete"
              color="#FF0000"
              onPress={() => deleteUser()}
            />
          </View>
        </Modal>
      </GestureRecognizer>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#FFFFFF",
    padding: 30,
    alignItems: "center",
  },
  subContainer: {
    marginBottom: 25,
    alignItems: "center",
  },
  title: {
    fontSize: 24,
  },
  subTitle: {
    fontSize: 20,
  },
  touchContainer: {
    backgroundColor: "#8CBBF1",
    padding: 15,
    borderRadius: 20,
    alignItems: "center",
  },
  deleteContainer: {
    backgroundColor: "#EC2424",
    padding: 15,
    borderRadius: 20,
    alignItems: "center",
  },
  touchText: {
    fontSize: 24,
    color: "#FFFFFF",
  },
  modalContainer: {
    flex: 1,
    alignItems: "center",
    justifyContent: "center",
    backgroundColor: "#FFFFFF",
    padding: 15,
  },
  closeButtonLoc: {
    alignItems: "flex-end",
  },
  modalTitle: {
    fontSize: 24,
    textAlign: "center",
  },
  modalSubTitle: {
    fontSize: 20,
    marginBottom: 30,
  },
  taskDayColor1: {
    backgroundColor: "#FDE18D",
    marginTop: 30,
    padding: 15,
    borderRadius: 30,
  },
  taskDayColor2: {
    backgroundColor: "#3D7D0B",
    marginTop: 30,
    padding: 15,
    borderRadius: 30,
  },
  taskDayColor3: {
    backgroundColor: "#E59010",
    marginTop: 30,
    padding: 15,
    borderRadius: 30,
  },
  currentDayColor1: {
    backgroundColor: "#D7DDE9",
    marginTop: 30,
    padding: 15,
    borderRadius: 30,
  },
  currentDayColor2: {
    backgroundColor: "#76A6F0",
    marginTop: 30,
    padding: 15,
    borderRadius: 30,
  },
  currentDayColor3: {
    backgroundColor: "#8CBBF1",
    marginTop: 30,
    padding: 15,
    borderRadius: 30,
  },
  input: {
    marginVertical: 15,
    paddingVertical: 15,
    paddingHorizontal: 15,
    width: 250,
    backgroundColor: "#FFF",
    borderRadius: 60,
    borderColor: "#C0C0C0",
    borderWidth: 1,
    fontSize: 16,
    textAlign: "center",
    marginBottom: 15,
  },
  modalCloseButton: {
    width: 40,
    height: 40,
    backgroundColor: "#FFF",
    borderRadius: 60,
    justifyContent: "center",
    alignItems: "center",
    borderColor: "#C0C0C0",
    borderWidth: 1,
    margin: 10,
  },
});

export default SettingsScreen;
