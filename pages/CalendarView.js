import React, { Component, useEffect, useState, useContext } from "react";
import { StyleSheet, Text, View } from "react-native";
import { Calendar, CalendarList } from "react-native-calendars";
import { LocaleConfig } from "react-native-calendars";
import moment from "moment";
import UserContext from "../UserContext";
import { useIsFocused } from "@react-navigation/native";

LocaleConfig.locales["fr"] = {
  monthNames: [
    "January",
    "February",
    "March",
    "April",
    "May",
    "June",
    "July",
    "August",
    "September",
    "October",
    "November",
    "December",
  ],
  monthNamesShort: [
    "Jan",
    "Feb",
    "Mar",
    "Apr",
    "May",
    "Jun",
    "Jul",
    "Aug",
    "Sept.",
    "Oct",
    "Nov",
    "Dec",
  ],
  dayNames: [
    "Sunday",
    "Monday",
    "Tuesday",
    "Wednesday",
    "Thursday",
    "Friday",
    "Saturday",
  ],
  dayNamesShort: ["Sun", "Mon", "Tues", "Wed", "Thurs", "Fri", "Sat"],
  today: "Today",
};
LocaleConfig.defaultLocale = "fr";

const CalendarView = ({ navigation }) => {
  //Get UUID from user context
  const { uuid, setUuid } = useContext(UserContext);

  //User Context for setting colors
  const { taskDayColor, setTaskDayColor } = useContext(UserContext);
  const { currentDayColor, setCurrentDayColor } = useContext(UserContext);

  //Hooks for tasks
  const [allTasks, setAllTasks] = useState([]);
  const [taskOwner, setTaskOwner] = useState(uuid);
  const [date, setDate] = useState("");
  const [selectDay, setSelectDay] = useState("");
  const [taskItems, setTaskItems] = useState([]);

  //Refreshes every time color changes

  const isFocused = useIsFocused();
  //Get all tasks of user
  useEffect(() => {
    setTaskOwner(uuid);
    fetch(`https://fierce-crag-15789.herokuapp.com/tasks/all`, {
      method: "POST",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
      },
      body: JSON.stringify({
        taskOwner: taskOwner,
      }),
    })
      .then((res) => res.json())
      .then((res) => {
        setAllTasks(res);
      });
  }, [isFocused]);

  //Setting all tasks in marked dates
  let markedDate = {};

  allTasks.map((task) => {
    markedDate[task._id] = {
      selected: true,
      selectedColor: taskDayColor,
    };
  });

  //Disabling previous dates
  const today = moment().format("YYYY-MM-DD");
  const todayLong = moment().format("MMMM Do");

  //Get tasks for today
  useEffect(() => {
    console.log(`current color from calendar view ${currentDayColor}`);
    setDate(today);
    fetch(`https://fierce-crag-15789.herokuapp.com/tasks/today`, {
      method: "POST",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
      },
      body: JSON.stringify({
        taskOwner: taskOwner,
        date: date,
      }),
    })
      .then((res) => res.json())
      .then((data) => {
        setTaskItems(data);
        console.log(taskItems);
      });
  }, [isFocused]);

  //Render Calendar View
  return (
    <View style={styles.container}>
      <Calendar
        style={{
          borderWidth: 1,
          borderColor: "gray",
          height: 350,
        }}
        theme={{
          backgroundColor: "#FFFFFF",
          calendarBackground: "#FFFFFF",
          textSectionTitleColor: "#b6c1cd",
          selectedDayBackgroundColor: "#FCEECB",
          selectedDayTextColor: "#ffffff",
          todayTextColor: "#FFFFFF",
          todayBackgroundColor: currentDayColor,
          dayTextColor: "#2d4150",
          textDisabledColor: "#d9e1e8",
          dotColor: "#00adf5",
          selectedDotColor: "#ffffff",
          arrowColor: "#FCEECB",
          monthTextColor: "black",
          indicatorColor: "blue",
          textDayFontFamily: "monospace",
          textMonthFontFamily: "monospace",
          textDayHeaderFontFamily: "monospace",
          textDayFontWeight: "300",
          textMonthFontWeight: "bold",
          textDayHeaderFontWeight: "300",
          textDayFontSize: 16,
          textMonthFontSize: 16,
          textDayHeaderFontSize: 16,
        }}
        maxDate={"3000-05-01"}
        onDayPress={(day) => {
          console.log("selected day", day);
          setSelectDay(day);
          navigation.navigate("TaskView", { day });
        }}
        markedDates={markedDate}
      />
      <View style={styles.tasks}>
        <Text style={styles.sectionTitle}>
          Pending Tasks Today: {todayLong}
        </Text>
        <View>
          {taskItems.length === 0 ? (
            <Text style={styles.subText}>
              You have no pending tasks today! Enjoy your day!
            </Text>
          ) : (
            taskItems.map((item) => {
              if (item.isDone === false) {
                return (
                  <Text style={styles.subText} key={item._id}>
                    {item.name} - {item.description}
                  </Text>
                );
              }
            })
          )}
        </View>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#FFFFFF",
  },
  tasks: {
    marginTop: 30,
  },
  sectionTitle: {
    fontSize: 24,
    marginLeft: 10,
    fontWeight: "bold",
  },
  subText: {
    marginTop: 15,
    marginLeft: 15,
    fontSize: 16,
  },
  emptyText: {
    fontSize: 16,
  },
});

export default CalendarView;
