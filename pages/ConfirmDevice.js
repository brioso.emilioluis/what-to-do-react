import React, { useState, useEffect, useContext } from "react";
import { Text, View, StyleSheet, TextInput, Button, Alert } from "react-native";
import * as Device from "expo-device";
import AsyncStorage from "@react-native-async-storage/async-storage";
import UserContext from "../UserContext";

const ConfirmDevice = ({ navigation }) => {
  //Declare hooks for new user registration
  let [deviceOwner, setDeviceOwner] = useState("");
  let [deviceType, setDeviceType] = useState("");
  let [deviceModel, setDeviceModel] = useState("");
  let [deviceOS, setDeviceOS] = useState("");
  const { uuid, setUuid } = useContext(UserContext);

  //Sets device Info using get device info npm
  useEffect(() => {
    AsyncStorage.getItem("user").then((value) => {
      setDeviceOwner(value);
    });
    setDeviceType(Device.osName);
    setDeviceModel(Device.modelName);
    setDeviceOS(`${Device.osName} ${Device.osVersion}`);
  }, []);

  //Add device function API
  const registerDevice = () => {
    console.log(`button works`);
    fetch(`https://fierce-crag-15789.herokuapp.com/users/create`, {
      method: "POST",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
      },
      body: JSON.stringify({
        deviceOwner: deviceOwner,
        deviceType: deviceType,
        deviceModel: deviceModel,
        deviceOS: deviceOS,
      }),
    })
      .then((res) => res.json())
      .then((data) => {
        console.log(data);
        Alert.alert(
          `Congratulations! You've registered your device`,
          `UUID: ${data._id}`,
          [
            {
              text: "Ok",
              onPress: () => {
                AsyncStorage.setItem("uuid", data._id);
                setUuid(data._id);
              },
              style: "default",
            },
          ]
        );
      })
      .catch((error) => {
        console.log(error);
      });
  };

  return (
    <View style={styles.container}>
      <Text style={styles.header}>Hello {deviceOwner}!</Text>
      <Text style={styles.subHeader}>
        Please confirm your device details below:
      </Text>
      <TextInput style={styles.input} value={deviceType} editable={false} />
      <TextInput style={styles.input} value={deviceModel} editable={false} />
      <TextInput style={styles.input} value={deviceOS} editable={false} />
      <Button
        color="#8CBBF1"
        title="Confirm"
        onPress={() => registerDevice()}
      />
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#FFFFFF",
    alignItems: "center",
    justifyContent: "center",
  },

  input: {
    height: 40,
    width: 150,
    margin: 12,
    borderWidth: 1,
    padding: 10,
    fontSize: 16,
  },
  header: {
    fontSize: 24,
  },
  subHeader: {
    fontSize: 16,
  },
});

export default ConfirmDevice;
