import React from "react";
import { View, Text, StyleSheet, Button } from "react-native";

const Task = (props) => {
  return (
    <View
      style={[props.props.item.isDone ? styles.itemDone : styles.itemPending]}
    >
      <View style={styles.itemLeft}>
        <View style={styles.square}></View>
        <View style={styles.text}>
          <Text style={styles.itemText}>{props.props.item.name}</Text>
          <Text style={styles.itemSubText}>{props.props.item.description}</Text>
        </View>
      </View>
      <View
        style={[
          props.props.item.isDone
            ? styles.circularDone
            : styles.circularPending,
        ]}
      ></View>
    </View>
  );
};

const styles = StyleSheet.create({
  itemPending: {
    backgroundColor: "#FFFFFF",
    borderColor: "#D7DDE9",
    borderWidth: 2,
    padding: 15,
    borderRadius: 10,
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "space-between",
    marginBottom: 20,
  },
  itemDone: {
    backgroundColor: "#FCEECB",
    padding: 15,
    borderRadius: 10,
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "space-between",
    marginBottom: 20,
  },
  itemLeft: {
    flexDirection: "row",
    alignItems: "center",
    flexWrap: "wrap",
  },
  square: {
    width: 24,
    height: 24,
    backgroundColor: "#55BCF6",
    opacity: 0.4,
    borderRadius: 5,
    marginRight: 15,
  },
  itemText: {
    maxWidth: "80%",
  },
  itemSubText: {
    opacity: 0.5,
  },
  text: {
    flexDirection: "column",
    flexWrap: "wrap",
  },
  circularPending: {
    width: 12,
    height: 12,
    borderColor: "#55BCF6",
    borderWidth: 2,
    borderRadius: 5,
  },
  circularDone: {
    width: 12,
    height: 12,
    borderColor: "#55BCF6",
    backgroundColor: "#55BCF6",
    borderWidth: 2,
    borderRadius: 5,
  },
  rightAction: {
    margin: 0,
    alignContent: "center",
    justifyContent: "center",
    width: 70,
  },
});

export default Task;
