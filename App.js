import React, { useState, useEffect } from "react";
import { Text, View, StyleSheet } from "react-native";
import { createBottomTabNavigator } from "@react-navigation/bottom-tabs";
import AsyncStorage from "@react-native-async-storage/async-storage";
import { createStackNavigator } from "@react-navigation/stack";

import { UserProvider } from "./UserContext";

// Screens
import TaskToday from "./pages/TaskToday";
import CalendarView from "./pages/CalendarView";
import StartUp from "./pages/StartUp";
import ConfirmDevice from "./pages/ConfirmDevice";
import SettingsScreen from "./pages/SettingsScreen";
import { NavigationContainer } from "@react-navigation/native";

//Task Screen
import Icon from "react-native-vector-icons/Ionicons";
const TaskView = createBottomTabNavigator();
function TaskViewFunction() {
  return (
    <TaskView.Navigator>
      <TaskView.Screen
        name="TaskView"
        options={{
          title: "Day View",
          headerStyle: {
            backgroundColor: "#8CBBF1",
            height: 100,
          },
          headerTintColor: "#fff",
          headerTitleStyle: {
            fontWeight: "bold",
            fontSize: 32,
          },
          tabBarIcon: (x) => {
            return <Icon name="today" size={24} />;
          },
        }}
        component={TaskToday}
      />
      <TaskView.Screen
        name="MonthView"
        options={{
          title: "Month View",
          headerStyle: {
            backgroundColor: "#8CBBF1",
            height: 100,
          },
          headerTintColor: "#fff",
          headerTitleStyle: {
            fontWeight: "bold",
            fontSize: 32,
          },
          tabBarIcon: (x) => {
            return <Icon name="calendar" size={24} />;
          },
        }}
        component={CalendarView}
      />
      <TaskView.Screen
        name="Settings"
        options={{
          title: "Settings",
          headerStyle: {
            backgroundColor: "#8CBBF1",
            height: 100,
          },
          headerTintColor: "#fff",
          headerTitleStyle: {
            fontWeight: "bold",
            fontSize: 32,
          },
          tabBarIcon: (x) => {
            return <Icon name="settings" size={24} />;
          },
        }}
        component={SettingsScreen}
      />
    </TaskView.Navigator>
  );
}

//Set Up Screen
const StartUpView = createStackNavigator();
function StartUpViewFunction() {
  return (
    <StartUpView.Navigator>
      <StartUpView.Screen
        name="StartUpScreen"
        component={StartUp}
        options={{
          title: "What To Do?",
          headerStyle: {
            backgroundColor: "#8CBBF1",
            height: 100,
          },
          headerTintColor: "#fff",
          headerTitleStyle: {
            fontWeight: "bold",
            fontSize: 32,
          },
        }}
      />
      <StartUpView.Screen
        name="ConfirmDeviceScreen"
        component={ConfirmDevice}
        options={{
          title: "What To Do?",
          headerStyle: {
            backgroundColor: "#8CBBF1",
            height: 100,
          },
          headerTintColor: "#fff",
          headerTitleStyle: {
            fontWeight: "bold",
            fontSize: 32,
          },
        }}
      />
    </StartUpView.Navigator>
  );
}

//Root Stack Screen
const RootStack = createStackNavigator();
const RootStackScreen = (uuid) => {
  console.log(uuid);
  return (
    <RootStack.Navigator>
      {uuid.uuid === null ? (
        <RootStack.Screen
          name="start"
          component={StartUpViewFunction}
          options={{ animationEnabled: false, headerShown: false }}
        />
      ) : (
        <RootStack.Screen
          name="tasks"
          component={TaskViewFunction}
          options={{ animationEnabled: false, headerShown: false }}
        />
      )}
    </RootStack.Navigator>
  );
};

export default function App() {
  const [taskDayColor, setTaskDayColor] = useState("#FDE18D");
  const [currentDayColor, setCurrentDayColor] = useState("#D7DDE9");
  const [uuid, setUuid] = useState(null);
  useEffect(() => {
    AsyncStorage.getItem("uuid").then((v) => {
      setUuid(v);
    });
  }, []);
  //const Nav = RootNavigator();
  return (
    <View style={styles.container}>
      <UserProvider
        value={{
          taskDayColor,
          setTaskDayColor,
          currentDayColor,
          setCurrentDayColor,
          uuid,
          setUuid,
        }}
      >
        <NavigationContainer>
          <RootStackScreen uuid={uuid} />
        </NavigationContainer>
        {/* <Nav /> */}
      </UserProvider>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff",
  },
});
